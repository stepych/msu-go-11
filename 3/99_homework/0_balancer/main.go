package main

import (
	"fmt"
	"sync"
)

type RoundRobinBalancer struct {
	stat []int
	node int
	lock sync.RWMutex
}

func (b *RoundRobinBalancer) Init(balancers int) {
	b.lock.Lock()
	defer b.lock.Unlock()
	b.stat = make([]int, balancers, balancers)
	b.node = -1
}

func (b *RoundRobinBalancer) GiveStat() []int {
	b.lock.RLock()
	defer b.lock.RUnlock()
	return b.stat
}

func (b *RoundRobinBalancer) GiveNode() int {
	b.lock.Lock()
	defer b.lock.Unlock()
	b.node++
	if b.node >= len(b.stat) {
		b.node = 0
	}
	b.stat[b.node]++
	return b.node
}

func main() {
	b := new(RoundRobinBalancer)
	b.Init(3)
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	go b.GiveNode()
	fmt.Scanln()
}
