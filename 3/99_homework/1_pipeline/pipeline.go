package pipeline
import "sync"

type job func(in, out chan interface{})

func Pipe(funcs ...job) {
	wg := sync.WaitGroup{}
	var out chan interface{}
	for _, f := range funcs {
		in := out
		out = make(chan interface{})
		wg.Add(1)
		go func(f job, in, out chan interface{}) {
			f(in, out)
			close(out)
			wg.Done()
		} (f, in, out)
	}
	wg.Wait()
	return
}
