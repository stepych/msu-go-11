package main

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
)

type crawler struct {
	links        []string
	linksCrawled map[string]struct{}
}

func Crawl(host string) (links []string) {
	crawler := crawler{linksCrawled: make(map[string]struct{})}
	crawler.crawlPage(host, "")
	return crawler.links
}

func (crawler *crawler) crawlPage(host string, page string) {
	if len(page) > 1 && []rune(page)[0] != '/' {
		page = "/" + page
	}
	body, realPage, statusCode := getHttp(host + page)
	if statusCode == 404 {
		return
	}
	crawler.markAsCrawled(page)
	crawler.markAsCrawled(realPage)

	links := allMatches(`<a href="([^"]*)"`, body)
	for _, link := range links {
		if _, crawled := crawler.linksCrawled[link]; !crawled && checkURLInHost(link, host) {
			crawler.crawlPage(host, link)
		}
	}
}

func checkURLInHost(urlStr, host string) bool {
	urlStruct, _ := url.Parse(urlStr)
	if urlStruct.Host == "" || urlStruct.Host == host {
		return true
	}
	return false
}

func (crawler *crawler) markAsCrawled(link string) {
	if _, crawled := crawler.linksCrawled[link]; !crawled && link != "" {
		crawler.linksCrawled[link] = struct{}{}
		crawler.links = append(crawler.links, link)
	}
}

func getHttp(host string) (body, realURL string, statusCode int) {
	resp, _ := http.Get(host)
	defer resp.Body.Close()
	realURL = resp.Request.URL.Path
	bodyRaw, _ := ioutil.ReadAll(resp.Body)
	return string(bodyRaw), realURL, resp.StatusCode
}

func allMatches(regexpStr string, str string) (results []string) {
	re := regexp.MustCompile(regexpStr)
	matches := re.FindAllStringSubmatch(str, -1)
	for _, submatches := range matches {
		url := submatches[1]
		results = append(results, url)
	}
	return
}
