package main

import (
	"fmt"
	"sort"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [...]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(slice []int) (str string) {
	for _, v := range slice {
		str += fmt.Sprintf("%d", v)
	}
	return
}

func MergeSlices(sl1 []float32, sl2 []int32) []int {
	sl3 := make([]int, 0, len(sl1)+len(sl2))
	for _, v := range sl1 {
		sl3 = append(sl3, int(v))
	}
	for _, v := range sl2 {
		sl3 = append(sl3, int(v))
	}
	return sl3
}

func GetMapValuesSortedByKey(m map[int]string) (vSorted []string) {
	ids := []int{}
	for id, _ := range m {
		ids = append(ids, id)
	}
	sort.Ints(ids)
	for _, id := range ids {
		vSorted = append(vSorted, m[id])
	}
	return
}

func main() {
}
